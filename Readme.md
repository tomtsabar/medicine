## About The Project

Medlookup.org is a non-profit project for indexing and of drugs leaflets and information.
The website is free of user for the public in https://medlookup.org.
The information and leaflets is based in the Israerly MOH database - https://israeldrugs.health.gov.il/.
The website supports Hebrew, English and Arabic.

## Getting Started
Download python - tested on versions: ['3.7']
Install pip

### Installation
pip install -m requirements.txt

### Running

Usage: website.py [OPTIONS]

Options:
  --debug / --no-debug  [required]
  --drugs-file TEXT     Path to a file contains drugs json  [required]
  --help                Show this message and exit.

#### For example:
python website.py --debug --drugs-file drugs_with_urls_10.4.2024.json

## Contact
Tom Tsabar - tomtsabar9@gmail.com

<p align="right">(<a href="#readme-top">back to top</a>)</p>

