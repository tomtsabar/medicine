
lang_dict = {'he': 'h', 'en': 'e', 'ar':'a', 'ru': 'r'}
drug_base_url = "https://mohpublic.z6.web.core.windows.net/IsraelDrugs/"
text_button = {'ar': 'النشرة باللغة العربية', 'he':'עלון לצרכן (עברית)', 'en':'Package Leaflet'}
text_doctor_button = {'ar': 'نشرة للطبيب', 'he':'עלון לרופא (אנגלית)', 'en':'Doctor leaflet'}
text_not_found_button = {'ar': 'لا توجد نشرة', 'he':'לא נמצא', 'en':'Not found'}
text_active_material = {'ar': 'المكونات النشطة: ', 'he':'חומרים פעילים:', 'en':'Active materials: '}

medicines_data = NaN
drug_names = NaN
ac_dict = NaN
combined_names = NaN


//Setting the controls
var search_bar = document.getElementById("search_bar");
search_bar.addEventListener("keydown", function(event) {
  // If the user presses the "Enter" key on the keyboard
  if (event.key === "Enter") {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("submit_drug").click();
  }
});

document.getElementById("quick_search").addEventListener('change', function() {
  if (this.checked) {
    document.getElementById("quick_search_text").style.color = "#7151cb";
    localStorage['quick']  = true;
  } else {
  	localStorage['quick']  = false;
    document.getElementById("quick_search_text").style.color = "#adaab8";
  }
});

document.getElementById("info_button").addEventListener('mouseenter', function() {
	info_button_text = document.getElementById("info_button_text");
	info_button = document.getElementById("info_button_img");
	info_blur = document.getElementById("blur");

	info_button_text.style.visibility = 'visible';
	info_blur.style.visibility = 'visible';
	info_button.src="/img/Info_Icon_Dark.svg";
	});

document.getElementById("blur").addEventListener('mousedown', function() {
	info_button_text = document.getElementById("info_button_text");
	info_button = document.getElementById("info_button_img");
	info_blur = document.getElementById("blur");

	info_button_text.style.visibility = 'hidden';
	info_blur.style.visibility = 'hidden';
	info_button.src="/img/Info_Icon.svg";
	
	});

document.getElementById("search_bar").addEventListener('mouseover', function() {
	search_bar = document.getElementById("search_bar")
	search_bar.style.setProperty("box-shadow","0 4px 8px 0 rgb(0 0 0 / 5%)");
	search_bar.style.setProperty("border", "0px solid #7151CB66");

	float_search_bar = document.getElementById("float_search_bar")
	float_search_bar.style.setProperty("visibility","visible");
	document.getElementById("found_label").style.visibility="hidden"
	});

document.getElementById("search_bar").addEventListener('mouseout', function() {
	search_bar = document.getElementById("search_bar")
	search_bar.style.removeProperty("box-shadow");
	search_bar.style.setProperty("border", "1px solid #7151CB66");

	float_search_bar = document.getElementById("float_search_bar")
	float_search_bar.style.setProperty("visibility","hidden");
	});

//Set the initial loading functions
var drug_xhttp = new XMLHttpRequest();
var ac_xhttp = new XMLHttpRequest();

function change_page_lang(lang)
{
	if (document.location.href.indexOf(get_current_lang()) != -1)
	{
		document.location.href = document.location.href.replace(get_current_lang(), lang)
	}
	else
	{
		hash_index = document.location.href.indexOf("#")
		hash = ""
		if (hash_index != -1)
		{
			hash =  document.location.href.slice(hash_index)
		}
		
		document.location.href = "/"+lang+"/index.html"+hash
	}
	
}

function load_active_materials()
	{
		result_json = JSON.parse(ac_xhttp.responseText);
	          
		ac_dict = result_json

		combined_names = Object.keys(ac_dict.am).concat(drug_names)
		//combined_names = drug_names

		autocomplete(document.getElementById("drug_input"), autocomplete_drug, combined_names);

		search_from_anchor()
	}

function load_drug_and_active_materials()
	{
		result_json = JSON.parse(drug_xhttp.responseText);
          
		drug_names = result_json['drugs']

		ac_xhttp.addEventListener("load", load_active_materials);
		ac_xhttp.open("GET", "/getAllAMnames");
		ac_xhttp.send()
	}

//Load drug and active materials
drug_xhttp.addEventListener("load", load_drug_and_active_materials);
drug_xhttp.open("GET", "/getAllDrugNames");
drug_xhttp.send()

function search_from_anchor()
{
	hash_index = document.location.href.indexOf("#")
	if (hash_index != -1)
	{
		document.getElementById("drug_input").value = decodeURI(document.location.href.slice(hash_index+1))
		load_drug_search_results()
	}
}