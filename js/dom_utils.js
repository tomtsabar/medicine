function create_bolded_text(text, bolded_indices, element_type)
	{
		if (bolded_indices == null)
		{
			bolded_indices = []
		}
		bolded_indices = bolded_indices.sort(function(a, b){return b-a});
		text = text.replaceAll(' ', '\xa0')
		text_wrapper = document.createElement('div');
		text_wrapper.className = element_type+'-wrapper'

		is_bold = false;
		next_bold = pop_or_minus_1(bolded_indices);

		next_text = "";

		for (let i = 0; i < text.length; i++) {
			if (is_bold == false)
			{
				if (next_bold != i)
				{
					next_text += text[i];
				}
				else
				{
					result_span = document.createElement('span');
					result_span.className = element_type+'-result'
					result_span.innerText = next_text.replaceAll('\xa0','\xa0\u200B');
					text_wrapper.appendChild(result_span);

					next_text = text[i];
					next_bold = pop_or_minus_1(bolded_indices);
					is_bold = true;
				}
				
			}
			else
			{
				if (next_bold != i)
				{
					result_span = document.createElement('span');
					result_span.className = element_type+'-result-bold'
					result_span.innerText = next_text.replaceAll('\xa0','\xa0\u200B');
					text_wrapper.appendChild(result_span);

					next_text = text[i];
					is_bold = false;
					
				}
				else
				{
					next_text += text[i];
					next_bold = pop_or_minus_1(bolded_indices);
				}
			}
		}

		if (is_bold == false)
			{
				result_span = document.createElement('span');
				result_span.className = element_type+'-result'
				result_span.innerText = next_text.replaceAll('\xa0','\xa0\u200B');
				text_wrapper.appendChild(result_span);
			}else{
				result_span = document.createElement('span');
				result_span.className = element_type+'-result-bold'
				result_span.innerText = next_text.replaceAll('\xa0','\xa0\u200B');
				text_wrapper.appendChild(result_span);
			}
		return text_wrapper;
	}

function change_lang(lang)
	{
		text = "English"
		if ('he' == lang)
		{
			text = "עברית"
		}
		if ('en' == lang)
		{
			text = "English"
		}
		if ('ar' == lang)
		{
			text = "العربية‬"
		}

		document.getElementById("display_lang").textContent = text;
	}


function autocomplete_drug()
	{
		if (document.getElementById("quick_search").checked)
		{
			document.getElementById("submit_drug").click();
		}
		else
		{
			load_drug_search_results()
		}
	}

function create_drug_button(drug_name)
	{
		result_pdf_button = document.createElement('button');
		result_pdf_button.type = 'submit';
		lang = get_current_lang();
		if (get_pdf(drug_name, false) != null)
			{
				result_pdf_button.className = 'button-pdf';
				result_pdf_button.innerText = text_button[lang];
				result_pdf_button.onclick = function() {send_med_name(drug_name, false)};
			}
			else
			{
				result_pdf_button.innerText = text_not_found_button[lang];
				result_pdf_button.className = 'button-pdf-grey';
			}
		if (lang == "en")
		{
			result_pdf_button.style = "float: right";
		}
		return result_pdf_button;

	}

function create_drug_doctor_button(drug_name)
	{
		result_pdf_doctor_button = document.createElement('button');
		result_pdf_doctor_button.type = 'submit';
		lang = get_current_lang();
		if (get_pdf(drug_name, true) != null)
			{
				result_pdf_doctor_button.className = 'button-pdf-doctor';
				result_pdf_doctor_button.innerText = text_doctor_button[lang];
				result_pdf_doctor_button.onclick = function() {send_med_name(drug_name, true)};
			}
			else
			{
				result_pdf_doctor_button.innerText = text_not_found_button[lang];
				result_pdf_doctor_button.className = 'button-pdf-doctor-grey';
			}
		if (lang == "en")
		{
			result_pdf_doctor_button.style = "left: auto; right: 20px; float: right";
		}
		return result_pdf_doctor_button;
	}

function create_single_card(drug_name, text_div, active_material_title,  active_material_div )
{
	result_div = document.createElement('div');
	result_div.className = 'search-result';

	result_pdf_button = create_drug_button(drug_name);
	result_doctor_pdf_button = create_drug_doctor_button(drug_name);
	
	if (is_mobile)
	{
		result_div.appendChild(text_div);
		result_div.appendChild(active_material_title);
		result_div.appendChild(active_material_div);
		result_div.appendChild(result_pdf_button);
		result_div.appendChild(result_doctor_pdf_button);

	}
	else
	{
		result_div.appendChild(result_pdf_button);
		result_div.appendChild(result_doctor_pdf_button);
		result_div.appendChild(text_div);
		result_div.appendChild(active_material_title);
		result_div.appendChild(active_material_div);
	}
	
	search_results_tag.appendChild(result_div);

}

function create_drug_card(drug_name, drug_meta_data)
{

	text_div = create_bolded_text(drug_name, drug_meta_data[1], 'drug-name');
	active_material_title = create_bolded_text(text_active_material[lang], null,'active-material-title');
	active_material_div = create_bolded_text(""+medicines_data[drug_name]['AM'], null,'active-material');

	create_single_card(drug_name, text_div, active_material_title, active_material_div)
}

function create_am_cards(am_name, drug_meta_data)
{
	Object.keys(am_data[am_name]).forEach((drug_name) => {
		text_div = create_bolded_text(drug_name, null, 'drug-name');

		active_material_title = create_bolded_text(text_active_material[lang], null,'active-material-title');


		active_material_text = ""+am_data[am_name][drug_name]['AM']

		indices = drug_meta_data[1];
		index = active_material_text.indexOf(am_name);
		indices = indices.map(v=> v+index);

		active_material_div = create_bolded_text(active_material_text, indices, 'active-material');
		create_single_card(drug_name, text_div, active_material_title, active_material_div)

		});

}

function get_drugs_short_dict(names, open_after)
{
	var drug_dict_xhttp = new XMLHttpRequest();
	lang = get_current_lang();
	is_mobile = mobileCheck();

	function load_drug_search_results_callback()
	{

	medicines_data = JSON.parse(drug_dict_xhttp.responseText)['drugs']
	am_data = JSON.parse(drug_dict_xhttp.responseText)['am']
	document.getElementById("search_bar").style="top: 12%;"
	document.getElementById("float_search_bar").style="top: 12%;"
	document.getElementById("title").style="visibility: hidden;"

	search_results_tag = document.getElementById("search_results");
	search_results_tag.innerHTML='';
	if (!search_results_tag.className.endsWith("-high"))
	{
		search_results_tag.className = search_results_tag.className + '-high'
	}
	
	top_results.forEach((drug_option) => {

		name = drug_option[0]
		meta_data = drug_option[1]
	

		if (name in medicines_data) {
			create_drug_card(name, meta_data)
		}

		if (name in am_data) {
			create_am_cards(name, meta_data)
		}
		
	});

		// Scroll to the top
		document.activeElement.blur();
		scroll(0,0);

		if (open_after != null)
		{
			send_med_name(open_after, false);
		}

		}
	drug_dict_xhttp.addEventListener("load", load_drug_search_results_callback);
	drug_dict_xhttp.open("POST", "/getDragsInfo"); 
	drug_dict_xhttp.send(JSON.stringify({'d': names}))

	

}
function extract_drug_arrays(top_results)
{
	names_array = []
	top_results.forEach((drug_option) => {
		names_array.push(drug_option[0])
	});

	return names_array

}

function add_drug_to_url(drug_name)
{
	current_url = document.location.href
	hash_index = current_url.indexOf("#")
	if (hash_index != -1)
	{
		drug_name = drug_name.replaceAll("%", "%25%0A")
		document.location.href = document.location.href.slice(0,hash_index)+"#" + drug_name
	}
	else
	{
		document.location.href += "#" + drug_name
	}
}

function load_drug_search_results(open_after = null)
	{
		med_name = document.getElementById("drug_input").value;
		add_drug_to_url(med_name)
		
		
		log_search(med_name)
		top_results = searchTopX(combined_names, med_name, 20).slice(0,10);

		get_drugs_short_dict(extract_drug_arrays(top_results), open_after)
 
	}

function get_current_lang()
  {
  const langs = ['ar', 'he', 'en']; 

	for (let lang of langs) {
		if (window.location.href.includes('/'+lang+'/')) {
		  return lang;
			}
		}

	return "en";
    	
  }

function get_pdf(drug_name, is_doctor)
  {
    	if (typeof (medicines_data[drug_name]) == 'undefined')
    	{
    		return null;
    	}

    	lang = get_current_lang()

    	if (is_doctor)
    	{
    		if (typeof (medicines_data[drug_name]['d']) == 'undefined')
		    {
		    		return null
		    }
		    return drug_base_url + medicines_data[drug_name]['d'].l
    	}
	    if (typeof (medicines_data[drug_name][lang_dict[lang]]) == 'undefined')
	    {
	    		return null
	    }

	    return drug_base_url + medicines_data[drug_name][lang_dict[lang]].l

  }

function open_button_clicked()
  {
    med_name = document.getElementById("drug_input").value;

    if (!document.getElementById("quick_search").checked)
		{

			load_drug_search_results();
		}
		else
		{
			
			drug_list = document.getElementById("drug_inputautocomplete-list")
	    if (null != drug_list){
	    	if (1 == drug_list.childElementCount)
	    		{
	    		med_name = drug_list.firstChild.textContent
	    		}
	    		
	    	}
	    load_drug_search_results(med_name);
		}
  }

function send_med_name(drug_name, is_doctor)
  {
  	log_pdf(drug_name, is_doctor);
    downloadURI(get_pdf(drug_name, is_doctor) , drug_name);
   }