/**
 * MIT License
 *
 * Copyright (c) 2018 Fabvalaaah - fabvalaaah@laposte.net
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * DISCLAIMER:
 * I am not responsible in any way of any consequence of the usage of this piece
 * of software. You are warned, use it at your own risks.
 */
const initMatrix = (s1, s2) => {
	  /* istanbul ignore next */
	  if (undefined == s1 || undefined == s2) {
	    return null;
	  }

	  let d = [];
	  for (let i = 0; i <= s1.length; i++) {
	    d[i] = [];
	    d[i][0] = i;
	  }
	  for (let j = 0; j <= s2.length; j++) {
	    d[0][j] = j;
	  }

	  return d;
	};

	const damerau = (i, j, s1, s2, d, change_letter_cost) => {
	  if (i > 1 && j > 1 && s1[i - 1] === s2[j - 2] && s1[i - 2] === s2[j - 1]) {
	    d[i][j] = Math.min.apply(null, [d[i][j], d[i - 2][j - 2] + change_letter_cost]);
	  }
	};

	const get_changed_indexes = (d_ch, s1, s2) => {
		k = 1;
		i = s1.length;
		j = s2.length;
		changes_indexes = [];

		while (i>0 && j>0) {
			k+=1;

			if (i == 0) 
			{
				j-=1;
				if (d_ch[i][j] == d_ch[i][j+1])
				{
					changes_indexes.push(j);
				}
				continue;
			}
			if (j == 0) 
			{
				i-=1;
				
				continue;
			}
			min_value = Math.min.apply(null, [
	        d_ch[i - 1][j],
	        d_ch[i][j - 1],
	        d_ch[i - 1][j - 1],
	      ]);
			current_value = d_ch[i][j];

			if (min_value == d_ch[i - 1][j])
			{
				i-=1;
			}
			else if (min_value == d_ch[i][j-1])
			{
				j-=1;
				if (current_value == min_value)
				{
					changes_indexes.push(j);
				}
			}
			else if (min_value == d_ch[i - 1][j-1])
			{
				i-=1;
				j-=1;
				if (current_value == min_value)
				{
					changes_indexes.push(j);
				}
			}

		}
		return changes_indexes

	}
	const distance = (s1, s2) => {
	  if (
	    undefined == s1 ||
	    undefined == s2 ||
	    "string" !== typeof s1 ||
	    "string" !== typeof s2
	  ) {
	    return -1;
	  }
	  
	  let d = initMatrix(s1, s2);
	  let d_ch = initMatrix(s1, s2);
	  /* istanbul ignore next */
	  if (null === d) {
	    return -1;
	  }
	  for (var i = 1; i <= s1.length; i++) {
	    let change_letter_cost;
	    for (let j = 1; j <= s2.length; j++) {
	      if (s1.charAt(i - 1) === s2.charAt(j - 1)) {
	        change_letter_cost = 0;
	      } else {
	        change_letter_cost = 1.75;
	      }

	      add_letter_cost = 0.2;

	      d[i][j] = Math.min.apply(null, [
	        d[i - 1][j] + 1,
	        d[i][j - 1] + add_letter_cost,
	        d[i - 1][j - 1] + change_letter_cost
	      ]);

	      d_ch[i][j] = Math.min.apply(null, [
	        d_ch[i - 1][j] + 1,
	        d_ch[i][j - 1] + add_letter_cost,
	        d_ch[i - 1][j - 1] + change_letter_cost
	      ]);
	      

	      damerau(i, j, s1, s2, d, change_letter_cost);
	    }
	  }

	  indexes = get_changed_indexes(d_ch, s1, s2);
	  
	  return [d[s1.length][s2.length], indexes];
	};

	const distanceProm = (s1, s2) =>
	  new Promise((resolve, reject) => {
	    let result = distance(s1, s2)[0];
	    if (0 <= result) {
	      resolve(result);
	    } else {
	      reject(result);
	    }
	  });

	const minDistanceProm = (s1, list) =>
	  new Promise((resolve, reject) => {
	    if (undefined == list || !Array.isArray(list)) {
	      reject(-1);
	      return;
	    } else if (0 === list.length) {
	      resolve(distance(s1, "")[0]);
	      return;
	    }

	    let min = -2;

	    list.forEach((s2) => {
	      let d = distance(s1, s2)[0];
	      if (-2 === min || d < min) {
	        min = d;
	      }
	    });

	    if (0 <= min) {
	      resolve(min);
	    } else {
	      reject(min);
	    }
	  });

function indent_indicies(indicies, substr1, str1) {

	indent = str1.indexOf(substr1);

	return indicies.map(a => a + indent);
}

function extendedDistance(s1, s2) {

	 if (
	    undefined == s1 ||
	    undefined == s2 ||
	    "string" !== typeof s1 ||
	    "string" !== typeof s2
	  ) {
	    return -1;
	  }

	  indecies = []

	  s1 = s1.toUpperCase()
	  s2 = s2.toUpperCase()

	  accumulated_distance = 0
	  factorial = 50
	  words_penalty = 1.5

	  arr1 = s1.split(" ")
	  arr2 = s2.split(" ")
	
	  common_length = Math.min.apply(null, [arr1.length, arr2.length])
	  max_length = Math.max.apply(null, [arr1.length, arr2.length])
	  let i = 0
	  for (; i < common_length; i++)
	  {
	  	distance_tuple = distance(arr1[i], arr2[i]);
	  	
	  	indecies = indecies.concat(indent_indicies(distance_tuple[1], arr2[i], s2));
	  	

	  	accumulated_distance += distance_tuple[0]
	  }

	  if (arr1.length > arr2.length)
	  {
	  	accumulated_distance += words_penalty*(max_length - common_length);
	  }
	  

	  for (; i < max_length; i++)
	  {
	  	if (i < arr1.length)
	  	{
	  		accumulated_distance += (arr1[i].length / factorial)
	  	}
	  	if (i < arr2.length)
	  	{
	  		accumulated_distance += (arr2[i].length / factorial)
	  	}
	  	
	  }

	  return [accumulated_distance, indecies]


}

class MaxHeap {
 constructor() {
     this.heap = [];
 }
 
 // Helper Methods
 getLeftChildIndex(parentIndex) { return 2 * parentIndex + 1; }
 getRightChildIndex(parentIndex) { return 2 * parentIndex + 2; }
 
 getParentIndex(childIndex) {
     return Math.floor((childIndex - 1) / 2);
 }
 
 hasLeftChild(index) {
     return this.getLeftChildIndex(index) < this.heap.length;
 }
 
 hasRightChild(index) {
     return this.getRightChildIndex(index) < this.heap.length;
 }
 
 hasParent(index) {
     return this.getParentIndex(index) >= 0;
 }
 
 leftChild(index) {
     return this.heap[this.getLeftChildIndex(index)][0];
 }
 
 rightChild(index) {
     return this.heap[this.getRightChildIndex(index)][0];
 }
 
 parent(index) {
     return this.heap[this.getParentIndex(index)][0];
 }
 
 swap(indexOne, indexTwo) {
     const temp = this.heap[indexOne];
     this.heap[indexOne] = this.heap[indexTwo];
     this.heap[indexTwo] = temp;
 }
 
 peek() {
     if (this.heap.length === 0) {
         return null;
     }
     return this.heap[0];
 }
  
 // Removing an element will remove the
 // top element with highest priority then
 // heapifyDown will be called 
 remove() {
     if (this.heap.length === 0) {
         return null;
     }
     const item = this.heap[0];
     this.heap[0] = this.heap[this.heap.length - 1];
     this.heap.pop();
     this.heapifyDown();
     return item;
 }
 
 add(item) {
     this.heap.push(item);
     this.heapifyUp();
 }
 
 heapifyUp() {
     let index = this.heap.length - 1;
     while (this.hasParent(index) && this.parent(index) < this.heap[index][0]) {
         this.swap(this.getParentIndex(index), index);
         index = this.getParentIndex(index);
     }
 }
 
 heapifyDown() {
     let index = 0;
     while (this.hasLeftChild(index)) {
         let largerChildIndex = this.getLeftChildIndex(index);
         if (this.hasRightChild(index) && this.rightChild(index) > this.leftChild(index)) {
             largerChildIndex = this.getRightChildIndex(index);
         }
         if (this.heap[index][0] > this.heap[largerChildIndex][0]) {
             break;
         } else {
             this.swap(index, largerChildIndex);
         }
         index = largerChildIndex;
     }
 }
  
 printHeap() {
     var heap =` ${this.heap[0][0]} `
     for(var i = 1; i<this.heap.length;i++) {
         heap += ` ${this.heap[i][0]} `;
     }
     console.log(heap);
 }
}
 

function searchTopX(names, name, x) {

	topX = {}
	var heap = new MaxHeap();
	names.forEach((drug_option) => {
		score_indicies = extendedDistance(name, drug_option)
		score = score_indicies[0]

		indices = score_indicies[1]
	  	if (Object.keys(topX).length  < x)
		  {
		  	topX[drug_option] = [score, indices]
		  	heap.add([score, drug_option, indices])
		  }
		else
			{
				temparr = heap.peek()
				max_score = temparr[0]
				key_max = temparr[1]

				if (score < max_score)
				{
					topX[drug_option] = [score, indices]
					heap.remove()
					heap.add([score, drug_option, indices])
					delete topX[key_max]

				}
			}
	});
	// Create items array
	var items = Object.keys(topX).map(function(key) {
	  return [key, topX[key]];
	});
	// Sort the array based on the second element
	// First elements with the same prefix as the name
	// If both have the same prefix then the longer
	// Else take the score

	items.sort(function(first, second) {
		first_start = first[0].toUpperCase().startsWith(name.toUpperCase())
		second_start = second[0].toUpperCase().startsWith(name.toUpperCase())

		if ((!first_start) && (!second_start))
		{
			return first[1][0] - second[1][0];
		}
		if (first_start && second_start)
		{
			return first[1][0] - second[1][0]
		}
		else
		{
			return second_start - first_start;
		}
	});

	return items

}