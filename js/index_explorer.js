
	


	if (localStorage['quick'] == 'true')
	{
		document.getElementById("quick_search").checked = true;
	}
	debug_data = NaN

	var xhttp = new XMLHttpRequest();

	function complete_drug()
	{
		if (document.getElementById("quick_search").checked)
		{
			document.getElementById("submit_drug").click();
		}
		
	}

	function load_drug_names()
	{
		result_json = JSON.parse(xhttp.responseText);
		debug_data = result_json;
		
		autocomplete(document.getElementById("drug"), complete_drug, result_json.drag_names);
	}
	
    xhttp.open("GET", "/getAllDragNames");
    xhttp.onreadystatechange = function () {
	    	if(xhttp.readyState === 4) {
	    		load_drug_names();
	    	}
	    }
    xhttp.send()

    function send_med_name()
    {
    	
    	var xhttp = new XMLHttpRequest();

    	med_name = document.getElementById("drug").value;

    	function load_drug_url()
		{
			result_json = JSON.parse(xhttp.responseText);
	          
			debug_data = result_json;
			
			flabel = document.getElementById("found_label");
			if (xhttp.status == 200)
			{
				flabel.style.display = 'inline-block';
				flabel.innerHTML = '';
				drag_pdf_url = "https://mohpublic.z6.web.core.windows.net/IsraelDrugs/" + result_json.drag_url
				downloadURI(drag_pdf_url , med_name, "found_label")
			}else
			{
				flabel.style.display = 'inline-block';
				flabel.innerHTML = 'לא נמצא עלון למשתמש';
			}

			document.getElementById("drug").value = '';
			
		}
	
	    
	 
	    med_name = med_name.replaceAll("/",'***slash***').replaceAll("\\",'***backslash***')
	    xhttp.open("GET", "/med/" + encodeURIComponent(med_name));
	    xhttp.onreadystatechange = function () {
	    	if(xhttp.readyState === 4) {
	    		load_drug_url();
	    	}
	    }
	    xhttp.send()

    }
	