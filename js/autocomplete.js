MIN_AUTOCOMPLETE_SCORE = 2.7

async function AsyncCloseAllLists(elmnt, inp) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
      x[i].parentNode.removeChild(x[i]);
    }
  }
}

async function input_function_killable(outer_object, inp, sbmt, arr, { signal } = {})
{
  /*close any already open lists of autocompleted values*/
    var a, b, i, val = outer_object.value;
    await AsyncCloseAllLists(inp);
    if (!val) { return false;}
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.createElement("DIV");
    a.setAttribute("id", outer_object.id + "autocomplete-list");
    a.setAttribute("class", "autocomplete-items");
    /*append the DIV element as a child of the autocomplete container:*/
    outer_object.parentNode.appendChild(a);
    /*for each item in the array...*/

    top_results = searchTopX(arr, val, 15).slice(0,8);

    for (i = 0; i < top_results.length; i++) {
      /*check if the item starts with the same letters as the text field value:*/
      if (top_results[i][1][0] > MIN_AUTOCOMPLETE_SCORE)
      {
        continue;
      }
      
      /*create a DIV element for each matching element:*/
      b = document.createElement("DIV");
      /*make the matching letters bold:*/
      var strong = document.createElement("strong");
      strong.innerText = top_results[i][0].substr(0, val.length);
      b.appendChild(strong)
      
      b.innerText += top_results[i][0].substr(val.length);
      var inputTag = document.createElement("input");
      inputTag.type = 'hidden';
      inputTag.value = top_results[i][0];
      b.appendChild(inputTag)
      /*insert a input field that will hold the current array item's value:*/
     
      /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
          /*insert the value for the autocomplete text field:*/
          inp.value = this.innerText;
          /*close the list of autocompleted values,
          (or any other open lists of autocompleted values:*/
          AsyncCloseAllLists(inp);

          sbmt();
      });
      a.appendChild(b);
    }
}

function autocomplete(inp, sbmt , arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/

  inp.addEventListener("input", function(e) {
      
    
      const ac = new AbortController();
      ac.abort(); // cancel the update
      input_function_killable(this, inp, sbmt, arr, { signal: ac.signal });
      
      
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
      x[i].parentNode.removeChild(x[i]);
    }
  }
}
/*execute a function when someone clicks in the document:*/
document.addEventListener("click", function (e) {
    closeAllLists(e.target);
});
}