import click
import flask
from flask import request, Response, send_from_directory, make_response, send_file,redirect
import requests
import json
import base64
from waitress import serve
import datetime
import io
import codecs

from flask_mobility import Mobility
from flask_mobility.decorators import mobile_template

url_get_specific_drug = 'https://israeldrugs.health.gov.il/GovServiceList/IDRServer/GetSpecificDrug'


def get_specific_drug_json(drug_reg_num):
    return {'drugRegNum':drug_reg_num}

def update_url_if_new(urls, lang, brochure):
    if lang in urls:
        if urls[lang]['date'] < brochure['updateDate']:
            urls[lang] = {'url': brochure['url'], 'date':  brochure['updateDate']}
    else:
        urls[lang] = {'url': brochure['url'], 'date':  brochure['updateDate']}

def unescape(name):
    return name.replace("***slash***", "/").replace("***backslash***", "\\")

def get_search_result(drug_name):
    pass

        
def create_gui( requests = requests, debug = True, drugs = None, drug_names = None, active_materials = None):
    scheme = "https://"
    if (debug):
        scheme = "http://"
 
    app = flask.Flask(__name__, instance_relative_config=True)
    Mobility(app)
    app.config['DEBUG'] = debug

    @app.route('/js/<path:path>')
    def send_js(path):
        return send_from_directory('js', path)

    @app.route('/original/css/<path:path>')
    def send_original_css(path):
        return send_from_directory('css', path)

    @app.route('/css/<path:path>')
    @mobile_template('{mobile/}')
    def send_css(path, template):
        return send_from_directory(f'css/{template}', path)

    @app.route('/img/<path:path>')
    def send_image(path):
        return send_from_directory('img', path)

    @app.route('/he/ad')
    def send_he_ad():
        args = request.args
        ad_key = args.get("keyword")
        if (ad_key != None):
            ad_key = ad_key.replace("עלון לצרכן", "")
            ad_key = ad_key.replace("תופעות לוואי", "")
            ad_key = ad_key.replace("הוראות", "")
            ad_key = ad_key.replace("אזהרות", "")
        else:
            ad_key=""

        response = redirect(f"{scheme}{request.headers.get('Host')}/he/index.html#{ad_key}", code=302)
        if (None == request.cookies.get('userID')):
            now_date = datetime.datetime.now()
            exp_date = now_date + datetime.timedelta(days=(365*10))
            response.set_cookie(f'userID', f'{str(now_date)} ad {args.get("keyword")}', expires = exp_date)

        return response

    @app.route('/he/basic_ad')
    def send_he_basic_ad():
        args = request.args
        ad_key = args.get("keyword")
        if (ad_key != None):
            ad_key = ad_key.replace("עלון לצרכן", "")
            ad_key = ad_key.replace("תופעות לוואי", "")
            ad_key = ad_key.replace("הוראות", "")
            ad_key = ad_key.replace("אזהרות", "")
        else:
            ad_key=""

        response = redirect(f"{scheme}{request.headers.get('Host')}/he/index.html", code=302)
        if (None == request.cookies.get('userID')):
            now_date = datetime.datetime.now()
            exp_date = now_date + datetime.timedelta(days=(365*10))
            cookie = f'{str(now_date)} basead {args.get("keyword")}'
            response.set_cookie(f'userID', cookie, expires = exp_date)
        return response

    @app.route('/he/<path:path>')
    def send_he(path):

        resp = make_response(send_from_directory('./html/he', "index.html"))
        if (None == request.cookies.get('userID')):
            now_date = datetime.datetime.now()
            exp_date = now_date + datetime.timedelta(days=(365*10))
            resp.set_cookie('userID', str(now_date), expires = exp_date)
        return resp

    @app.route('/en/<path:path>')
    def send_en(path):
        resp = make_response(send_from_directory('./html/en', "index.html"))
        if (None == request.cookies.get('userID')):
            now_date = datetime.datetime.now()
            exp_date = now_date + datetime.timedelta(days=(365*10))
            resp.set_cookie('userID', str(now_date), expires = exp_date)
        return resp

    @app.route('/ar/<path:path>')
    def send_ar(path):
        resp = make_response(send_from_directory('./html/ar', "index.html"))
        if (None == request.cookies.get('userID')):
            now_date = datetime.datetime.now()
            exp_date = now_date + datetime.timedelta(days=(365*10))
            resp.set_cookie('userID', str(now_date), expires = exp_date)
        return resp

    @app.route('/google7169fb18e7e18090.html')
    def send_favicon():
        return make_response(send_from_directory('google', "google7169fb18e7e18090.html"))

    @app.route('/favicon.ico')
    def send_google():
        return make_response(send_from_directory('img', "favicon.ico"))

    @app.route('/sitemaps/<path:path>')
    def send_sitemap(path):
        return make_response(send_from_directory('sitemap', f"sitemap_{path}"))

    @app.route('/', methods=['GET'] )
    def home():

        referer = request.headers.get("Referer")
        if None == referer:
            referer = "None"
        else:
            referer.replace(",","...")

        host = request.headers.get("Host").replace(",","...")
        fullurl = request.url.replace(",","...")
        user_agent = request.headers.get("User-Agent").replace(",","...")
        ip = request.headers.get("X-Real-IP")
        user_id = request.cookies.get('userID')
        if ip == None:
            ip = "None"
        else:
            ip.replace(",","...")

        t = str(datetime.datetime.now())
        with open('./logs/first_enter.txt', 'a', encoding='utf-8') as f:
            f.write(f'host (\\): {host}, url: {fullurl} referer: {referer}, user-agent: {user_agent}, ip: {ip}, time {t}, userID {user_id}\n')


        if "Windows NT 5" in (request.headers.get('User-Agent')):
            response = make_response(send_from_directory('./html/', 'index_explorer.html'))

            with open('./logs/explorer.txt', 'a') as f:
                f.write(f'IP: {request.remote_addr} user-agent:{request.headers.get("User-Agent")}\n')
            return response

        if "Windows NT 6" in (request.headers.get('User-Agent')):

            response = make_response(send_from_directory('./html/', 'index_explorer.html'))
            with open('./logs/explorer.txt', 'a') as f:
                f.write(f'IP: {request.remote_addr} user-agent:{request.headers.get("User-Agent")}\n')
            return response

        supported_languages = ["en", "he", "ar"]
        lang = request.accept_languages.best_match(supported_languages)

        if (lang == "en"):
            return send_en("index.html")

        response = redirect(f"{scheme}{request.headers.get('Host')}/{lang}/index.html", code=302)
        return response

    @app.route('/', defaults={'path': ''})
    @app.route('/<path:path>')
    def default(path):

        referer = request.headers.get("Referer")
        if None == referer:
            referer = "None"
        else:
            referer.replace(",","...")

        host = request.headers.get("Host").replace(",","...")
        fullurl = request.url.replace(",","...")
        user_agent = request.headers.get("User-Agent").replace(",","...")
        ip = request.headers.get("X-Real-IP")
        user_id = request.cookies.get('userID')
        if ip == None:
            ip = "None"
        else:
            ip.replace(",","...")

        t = str(datetime.datetime.now())
        with open('./logs/first_enter.txt', 'a') as f:
            f.write(f'host (default): {host}, url: {fullurl} referer: {referer}, user-agent: {user_agent}, ip: {ip}, time {t}, userID {user_id}\n')

        supported_languages = ["en", "he", "ar"]
        lang = request.accept_languages.best_match(supported_languages)
        if (str(lang) == "None"):
            lang = "en"

        if (lang == "en"):
            return send_en("index.html")

        response = redirect(f"{scheme}{request.headers.get('Host')}/{lang}/index.html", code=302)
        return response

    @app.route('/insulin', methods=['GET'] )
    def insulin():
        return redirect("https://tsabar.ams3.cdn.digitaloceanspaces.com/index.html", code=302)

    @app.route('/getAllDrugNames', methods=['GET'] )
    def get_all_drug_name():
        return json.dumps({'drugs':drug_names}), 200, {'ContentType':'application/json'}

    @app.route('/getAllAMnames', methods=['GET'] )
    def get_all_ac_name():
        return json.dumps({'am':active_materials['en']}), 200, {'ContentType':'application/json'}

    @app.route('/getDragsInfo', methods=['POST'] )
    def get_specific_drug_info():

        request_drugs = json.loads(request.data.decode("utf-8"))
        response_dict = {'drugs': {}, 'am':{}}
        for request_drug_am in request_drugs['d']:

            if request_drug_am in drugs:
                response_dict['drugs'][request_drug_am] = drugs[request_drug_am]
            if request_drug_am in active_materials['en']:
                response_dict['am'][request_drug_am] = dict()
                for request_drug_in_am in active_materials['en'][request_drug_am]:
                    if (request_drug_in_am in drugs):
                        response_dict['am'][request_drug_am][request_drug_in_am] = drugs[request_drug_in_am]
                        response_dict['drugs'][request_drug_in_am] = drugs[request_drug_in_am]

        return json.dumps(response_dict), 200, {'ContentType':'application/json'}


    @app.route('/drugsearch/<drug_name>/<pref_lang>', methods=['GET'] )
    def log_drug_search(drug_name, pref_lang):
        ip = request.headers.get("X-Real-IP")
        if ip == None:
            ip = "None"
        else:
            ip.replace(",","...")
        uagent = request.headers.get("User-Agent").replace(",","...")
        t = str(datetime.datetime.now())
        lang = pref_lang.replace(",","...")
        alang = request.headers.get("Accept-Language").replace(",","...")
        host = request.headers.get("Host").replace(",","...")
        user_id = request.cookies.get('userID')

        meta_data = f'Text {drug_name}, IP: {ip}, user-agent:{uagent}, Lang: {lang}, Accepted-langs {alang}, Host: {host}, user_id {user_id}, Time: {datetime.datetime.now()}\n'
        
        with open('./logs/drug_queries.txt', 'a', encoding='utf-8') as f:
            f.write(meta_data)
            return json.dumps({'logged':'ok'}), 200, {'ContentType':'application/json'}


    @app.route('/drugpdf/<drug_name>/<pref_lang>/<is_doctor>', methods=['GET'] )
    def log_drug_pdf(drug_name, pref_lang, is_doctor):
        ip = request.headers.get("X-Real-IP")
        if ip == None:
            ip = "None"
        else:
            ip.replace(",","...")
        uagent = request.headers.get("User-Agent").replace(",","...")
        t = str(datetime.datetime.now())
        lang = pref_lang.replace(",","...")
        alang = request.headers.get("Accept-Language").replace(",","...")
        host = request.headers.get("Host").replace(",","...")
        user_id = request.cookies.get('userID')

        meta_data = f'Drug {drug_name}, IP: {ip}, user-agent:{uagent}, Lang: {lang}, Accepted-langs {alang}, Host: {host}, user_id {user_id}, is_doctor {is_doctor}, Time: {datetime.datetime.now()}\n'
        
        with open('./logs/drug_pdf_open.txt', 'a', encoding='utf-8') as f:
        
            f.write(meta_data)
            return json.dumps({'logged':'ok'}), 200, {'ContentType':'application/json'}
    return app

@click.command()
@click.option('--debug/--no-debug', required=True)
@click.option('--drugs-file',help='Path to a file contains drugs json', required=True)
@click.option('--am-file',help='Path to save the active materials json', required=True)
def click_run(debug, drugs_file, am_file):
    drugs = None
    active_materials = None
    with io.open(drugs_file, mode='r', encoding='utf-8') as f:
        drugs = json.load(f)

    drug_names = list(drugs.keys())

    with io.open(am_file, mode='r', encoding='utf-8') as f:
        active_materials = json.load(f)


    app = create_gui(requests, debug, drugs, drug_names, active_materials)
    if debug:
        #app.run(host='0.0.0.0', port='80', debug)
        serve(app, host="0.0.0.0", port=80, url_scheme='http')
    else:
        serve(app, host="0.0.0.0", port=8080, url_scheme='https')


if __name__ == '__main__':
    click_run()





