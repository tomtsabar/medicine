import click
import requests
import json
import os
import time
import io

url_get_drugs = 'https://israeldrugs.health.gov.il/GovServiceList/IDRServer/SearchByName'
url_get_specific_drug = 'https://israeldrugs.health.gov.il/GovServiceList/IDRServer/GetSpecificDrug'

doctor_heb = 'לרופא'
user_heb = 'לצרכן'
langs = ['d', 'h', 'e', 'a', 'r']
lang_he = 'עברית'
lang_en = 'אנגלית'
lang_ar = 'ערבית'
lang_ru = 'רוסית'
lang_ml = 'רב שפתי'

def prepare_requesst_json(letter, index):
    return {'val':letter,"prescription":False,"healthServices":False,"pageIndex":index,"orderBy":0}

def get_specific_drug_json(drug_reg_num):
    return {'dragRegNum':drug_reg_num}

def get_specific_drug_info(drug_reg_num):
    """
    Request all the information about a specific drug (drug_reg_num)
    Parse the response and return it in the drug_data data structure
    """
    
    json_parameters = get_specific_drug_json(drug_reg_num)

    drug_data = requests.post(url_get_specific_drug, json = json_parameters, verify=True)

    drug_data_json = drug_data.json()

    drug_data = dict()
    brochure_dict = dict()
    urls = dict()
    preference_order = []

    if 'brochure' in drug_data_json:
        brochure_array = drug_data_json['brochure']
        for brochure in brochure_array:

            if 'type' not in brochure or brochure['type'] is None:
                continue


            if 'lng' not in brochure or brochure['lng'] is None:
                if user_heb in brochure['type']:
                    drug_data['h'] = {'l':brochure['url'], 'd': brochure['updateDate']}
                elif doctor_heb in brochure['type']:
                    drug_data['d'] = {'l':brochure['url'], 'd': brochure['updateDate']}
            else:
                if doctor_heb in brochure['type']:
                    drug_data['d'] = {'l':brochure['url'], 'd': brochure['updateDate']}
                elif user_heb in brochure['type'] and lang_he in brochure['lng']:
                    drug_data['h'] = {'l':brochure['url'], 'd': brochure['updateDate']}
                elif user_heb in brochure['type'] and lang_en in brochure['lng']:
                    drug_data['e'] = {'l':brochure['url'], 'd': brochure['updateDate']}
                elif user_heb in brochure['type'] and lang_ar in brochure['lng']:
                    drug_data['a'] = {'l':brochure['url'], 'd': brochure['updateDate']}
                elif user_heb in brochure['type'] and lang_ru in brochure['lng']:
                    drug_data['r'] = {'l':brochure['url'], 'd': brochure['updateDate']}
                elif user_heb in brochure['type'] and lang_ml in brochure['lng']:
                    if ('a' not in drug_data):
                        drug_data['a'] = {'l':brochure['url'], 'd': brochure['updateDate']}
                    if ('e' not in drug_data):
                        drug_data['e'] = {'l':brochure['url'], 'd': brochure['updateDate']}
                    if ('h' not in drug_data):
                        drug_data['h'] = {'l':brochure['url'], 'd': brochure['updateDate']}

    if 'dragIndication' in drug_data_json:
        drug_data['DI'] = drug_data_json['dragIndication']

    if 'activeMetirals' in drug_data_json:
        materials = []
        for activeMaterial in drug_data_json['activeMetirals']:
            if 'ingredientsDesc' in activeMaterial:
                materials.append(activeMaterial['ingredientsDesc'])
            
        drug_data['AM'] = materials

    return drug_data

def update_drugs_json(drug_name, drugs, drug_data):
    """
    Update drug data in the drug json db.
    If the drug already exists it takes the most updated data (['d'] - date)
    """
    if (drug_name not in drugs):
        drugs[drug_name] = drug_data
    else:
        for lang in langs:
            if lang not in drug_data:
                continue
            if lang not in drugs[drug_name]:
                drugs[drug_name][lang] = drug_data[lang]
            elif drug_data[lang]['d'] > drugs[drug_name][lang]['d']:
                drugs[drug_name][lang] = drug_data[lang]

def update_active_materials_json(drug_name, active_materials, drug_data, lang):
    """
    Update drug data in the drug json db.
    If the drug already exists it takes the most updated data (['d'] - date)
    """
    if 'AM' in drug_data:
        for active_material in drug_data['AM']:
            if active_material in active_materials[lang]:
                active_materials[lang][active_material].append(drug_name)
            else:
                active_materials[lang][active_material] = [drug_name]



def handle_drug_data(drug_data_json, drugs, active_materials):
    """
    Parse the response json (drug_data_json) and add the drugs to the drugs db (drugs)
    The function acctually just take the drugRegNum and pass it to get_specific_drug_info.
    get_specific_drug_info creates another request and that is where the parsing really happen.
    """
    if 'results' not in drug_data_json:
        return True

    if len(drug_data_json['results']) <= 0:
        return True

    response_drugs_array = drug_data_json['results']

    # Iterate over all the drugs in the response
    for drug in response_drugs_array:
        drug_reg_num = drug['dragRegNum'] # Yes the mistake "drag" is right

        drug_data = get_specific_drug_info(drug_reg_num)

        if ('dragEnName' in drug.keys()): # Yes the mistake "drag" is right
            drug_english_name = drug['dragEnName'].lstrip(' ').strip()
            update_drugs_json(drug_english_name, drugs, drug_data)
            update_active_materials_json(drug_english_name, active_materials, drug_data, 'en')

        if ('dragHebName' in drug.keys()): # Yes the mistake "drag" is right
            drug_heb_name = drug['dragHebName'].lstrip(' ').strip()
            update_drugs_json(drug_heb_name, drugs, drug_data)
            update_active_materials_json(drug_heb_name, active_materials, drug_data, 'he')

    return False

@click.command()
@click.option('--drugs-file',help='Path to save the drugs json', required=True)
@click.option('--am-file',help='Path to save the active materials json', required=True)
def get_drug_data(drugs_file, am_file):

    drugs = dict()
    active_materials = {'he':dict(),'en':dict()}
    letters = '' # All
    index = 1 # Count, there are approximately 501 pages
    

    last_page = False
    while (not last_page):
        #try:
        json_parameters = prepare_requesst_json(letters, index)
        try:
            drug_data = requests.post(url_get_drugs, json = json_parameters)
        except Exception as e:
            print ("To much ssl problems, sleeping for 40 secs")
            print (e)
            time.sleep(40)
            continue

        drug_data_json = dict()
        try:
            drug_data_json = drug_data.json()
            last_page = handle_drug_data(drug_data_json, drugs, active_materials)
            print ("finished", index)
            index += 1
            
        except Exception as e:
            print (drug_data._content)
            print (e)


        #except Exception as e:
        #    time.sleep(2)
        #    print ("**********")
        #    print (e)
        #    print (letters, index)

    try:
        os.remove(drugs_file)
    except:
        pass

    language_count = {}
    print ("Amount of drugs: ", len(drugs))
    for drug in drugs.keys():
        for language in drugs[drug]:
            if language in language_count:
                language_count[language] += 1
            else:
                language_count[language] = 1
       
    for language in language_count.keys():
        print (f"Amount of drugs in {language}: ", language_count[language])


    with io.open(drugs_file, mode='w', encoding='utf-8') as f:
        json.dump(drugs, f, ensure_ascii=False)

    with io.open(am_file, mode='w', encoding='utf-8') as f:
        json.dump(active_materials, f, ensure_ascii=False)


get_drug_data()



