## About The Crawler

The crawler script reads all the information from https://israeldrugs.health.gov.il.
It looks up every drug, finds the urls of its leaflets and save it in a json data structure.

## Drugs data structure (json)

```
{
  "drug_name": {
    "e": { // English
      "l": "Rishum01_7_171022423.pdf", // Url Suffix
      "d": 1683559816000 // Last update
    },
    "h": { // Hebrew
      "l": "Rishum01_7_171022523.pdf", // Url Suffix
      "d": 1683559828000 // Last update
    },
    "a": { // Arabic
      "l": "Rishum01_7_171022323.pdf", // Url Suffix
      "d": 1683559802000 // Last update
    },
    "d": { // Doctor Leaflet
      "l": "Rishum01_7_171021823.pdf", // Url Suffix
      "d": 1683559784000 // Last update
    }
  }
}
```

## Getting Started
Download python - tested on versions: ['3.7']
Install pip

### Installation
pip install -m requirements.txt

### Running

Usage: crawler.py [OPTIONS]

Options:
  --drugs-file TEXT  Path to save the drugs json  [required]
  --help             Show this message and exit.


#### For example (running from the project root directory:
python crawler\crawler.py --drugs-file drugs_with_urls_7.5.2024.json

## Contact
Tom Tsabar - tomtsabar9@gmail.com

<p align="right">(<a href="#readme-top">back to top</a>)</p>

